[CmdletBinding()]
param(
    $lokaliseToken, $lokaliseDownloadedTranslationsFolder, $bitbucketUsername, $bidoneProductsFilePath, $bitbucketEmailAddress, 
    $lokaliseFileIndentation, $gitCommitMessage, $bitbucketAppPassword, $bitbucketWorkspace, $releaseMode
)

# powershell variables are passed as strings
# $bidoneProductsFilePath is a folder to get bidoneproducts json file
$projects=  Get-Content -Path $bidoneProductsFilePath | ConvertFrom-Json

Write-Output "STEP 1: Checking if '$lokaliseDownloadedTranslationsFolder' folder exists...`n`n"
if (Test-Path -Path $lokaliseDownloadedTranslationsFolder)
{
    Write-Output "Deleting '$lokaliseDownloadedTranslationsFolder'...`n"
    Remove-Item -Force -Recurse -Path $lokaliseDownloadedTranslationsFolder
}

Write-Output "STEP 2: Downloading Lokalise translation files`n`n"
foreach($project in $projects.projects)
{
    if(-Not $project.skipProcessing)
    {
        Write-Output "##############################################################################################################################`n"
        Write-Output "-- PROJECT '$($project.name)' --`n"
        Write-Output "##############################################################################################################################`n"

        foreach($component in $project.components)
        {
            if(-Not $component.skipProcessing)
            {
                if($component.hasSubComponents)
                {
                    Write-Output "'$($component.name)' - HAS SUBCOMPONENTS, downloading files from Lokalise using multiple files per language (original filenames and directory structure)...`n"
                    
                    .\lokalise2.exe `
                    --token $lokaliseToken `
                    --project-id $component.lokaliseProjectId `
                    file download `
                    --indentation $lokaliseFileIndentation `
                    --format $component.translationFileFormat `
                    --directory-prefix "./$lokaliseDownloadedTranslationsFolder/$($project.name)/$($component.name)/"  `
                    --json-unescaped-slashes=true `
                    --replace-breaks=false `
                    --export-empty-as base                    
                }
                else 
                {
                    Write-Output "`n'$($component.name)' - NO SUBCOMPONENTS, downloading files from Lokalise using one file per language...`n"

                    .\lokalise2.exe `
                    --token $lokaliseToken `
                    --project-id $component.lokaliseProjectId `
                    file download `
                    --indentation $lokaliseFileIndentation `
                    --format $component.translationFileFormat `
                    --original-filenames=false `
                    --bundle-structure "./$lokaliseDownloadedTranslationsFolder/$($project.name)/$($component.name)/$($component.prependFilenameText)%LANG_ISO%.%FORMAT%" `
                    --json-unescaped-slashes=true `
                    --replace-breaks=false `
                    --export-empty-as base
                }
            }
        }
    }
}

foreach($project in $projects.projects)
{
    if(-Not $project.skipProcessing)
    {
        Write-Output "##############################################################################################################################`n`n"
        Write-Output "-- PROJECT '$($project.name)' START --`n`n"
        Write-Output "##############################################################################################################################`n`n"

        Write-Output "`nSTEP 3: Checking if '$($project.translationsRepository)' folder exists...`n`n"
        if (Test-Path -Path $project.translationsRepository) {
            Write-Output "Deleting '$($project.translationsRepository)'..."
            # delete if exists so we always pull latest version of $translationsRepository
            Remove-Item -Force -Recurse -Path $project.translationsRepository
        }

        Write-Output "STEP 4: Cloning latest version of '$($project.translationsRepository)'..."
        git clone "https://$($bitbucketUsername):$($bitbucketAppPassword)@bitbucket.org/$($bitbucketWorkspace)/$($project.translationsRepository).git"
        cd $project.translationsRepository
        git checkout $project.branchToMergeInto

        git config --global user.email $bitbucketEmailAddress
        git config --global user.name $bitbucketUsername
        cd ..

        # loop each component (adminv5, bdirect web, identity server, etc.)
        foreach($component in $project.components)
        {
            if(-Not $component.skipProcessing)
            {
                Write-Output "************************************************************`n"
                Write-Output "-- COMPONENT '$($component.name)' START --`n"
                Write-Output "************************************************************`n"

                if($component.hasSubComponents)
                {
                    Write-Output "HAS SUBCOMPONENTS"

                    $sourceRootFolder = "$($lokaliseDownloadedTranslationsFolder)\$($project.name)\$($component.name)\$($component.pathToSaveTo)"
                    $subComponentFolders = dir $sourceRootFolder -r  | % { if ($_.PsIsContainer) { $_.Name }}
                    
                    foreach($subComponentFolder in $subComponentFolders)
                    {
                        Write-Output "`STEP 5: PROCESSING '$subComponentFolder'"
                        
                        <# 
                        this is the folder containing the translation files we downloaded from Lokalise (per subcomponent)
                        subcomponents are like the tables AdminReportColumn, AdminSortBy, EmailTemplateSection in BDirect DB
                        #>
                        $sourceFolder = "$sourceRootFolder\$subComponentFolder"

                        <#
                        this is the folder in the bitbucket repository where we will move the downloaded translation files to
                        we move files per subcomponent
                        #>
                        $destinationFolder = "$($project.translationsRepository)\$($component.pathToSaveTo)\$subComponentFolder"

                        # TODO: move this to a function
                        # when downloaded, the base file ("en" in this case) for resx files in Lokalise do not include the LANG_ISO in the file name
                        # for example, this means that SharedResource.en.resx becomes SharedResource.resx
                        # to fix this, we need to rename the base file and put in the LANG_ISO (en) manually
                        if($component.translationFileFormat -eq "resx")
                        {
                            Write-Output "`STEP 5.1: Project with '$($component.translationFileFormat)' file format detected, generating new filename with base LANG_ISO '$($component.baseLangIso)''"

                            $translationFiles = dir $sourceFolder -r  | % { if (-Not $_.PsIsContainer) { $_.FullName }}
                            foreach($translationFile in $translationFiles)
                            {
                                # CASE 1: file with LANG_ISO WITH culture
                                # e.g. if $splitFullNameArray is D:\a\1\s\lokalise-translations\Opri\Resources.en_AU.resx, it becomes ["D:\a\1\s\lokalise-translations\Opri\Resources", "en_AU", "resx"]

                                # CASE 2: file with LANG_ISO WITHOUT culture
                                # e.g. if $splitFullNameArray is D:\a\1\s\lokalise-translations\Opri\Resources.de.resx, it becomes ["D:\a\1\s\lokalise-translations\Opri\Resources", "de", "resx"]

                                # CASE 3: file without LANG_ISO
                                # e.g. if $splitFullNameArray is D:\a\1\s\lokalise-translations\Opri\Resources.resx, it becomes ["D:\a\1\s\lokalise-translations\Opri\Resources", "resx"]
                                $splitTranslationFileArray = $translationFile.Split(".")
                                $secondToTheLastItemIndex = $splitTranslationFileArray.Length - 2
                                $potentialLangISOCode = $splitTranslationFileArray[$secondToTheLastItemIndex]

                                # we then check if the second to the last item in the array contains an underscore "_"
                                # if it does, then it is is the LANG_ISO and we move to the next item in the list
                                # if it it does not, it can either be another LANG_ISO WITHOUT a culture (lt, lv, fr) OR the name before the LANG_ISO (Resources, SharedResources, etc.)
                                # for BidOne, because we always use a 2 character LANG_ISO code, we determine if it is the file that needs to be renamed based on this (if in the future, we support 3 character LANG_ISO codes, this logic needs to be changed)
                                # if the character count is greater than 2, we need to append the base LANG_ISO (en) LANG_ISO to the file name (insert it as the second last item in the array and combine them all again into one string)
                                if ((-Not $potentialLangISOCode.Contains("_")) -And ($potentialLangISOCode.Length -gt 2))
                                {
                                    Write-Output "`STEP 5.2: Updating '$translationFile' filename to include base LANG_ISO '$($component.baseLangIso)'"

                                    # construct a new array using the data of the original array
                                    $newTranslationFileArray = @()
                                    for($i = 0; $i -lt $splitTranslationFileArray.Length; $i++)
                                    {
                                        # if the current iteration is the last index, append the base LANG_ISO (en) first as the second to the last item in the array before the last item
                                        if($i -eq $splitTranslationFileArray.Length - 1)
                                        {
                                            $newTranslationFileArray += $($component.baseLangIso)
                                        }
                                        $newTranslationFileArray += $splitTranslationFileArray[$i]
                                    }

                                    # so based on the example above, for CASE 3, it now becomes ["D:\a\1\s\lokalise-translations\Opri\Resources", "en", "resx"]
                                    # we then rename the file with the newly added base LANG_ISO
                                    $translationFileWithBaseLangIso = $newTranslationFileArray -join '.'
                                    Rename-Item -Path $translationFile -NewName $translationFileWithBaseLangIso
                                }
                            }       
                        }

                        Write-Output "`STEP 6: Moving the below translation files FROM '$sourceFolder' TO '$destinationFolder'..."
                        $translationFiles = dir $sourceFolder -r  | % { if (-Not $_.PsIsContainer) { $_.Name }}
                        Write-Output $translationFiles
                        Copy-Item -Path "$($sourceFolder)\*.$($component.fileExtension)" -Destination $destinationFolder -Recurse

                        if($releaseMode)
                        {
                            Write-Output "`STEP 7: Committing $($component.name) - $subComponentFolder translation files to local repository..."
                            cd $project.translationsRepository
                            git add --all
                            git commit -m "AZDEVOPS - $($component.name) - $subComponentFolder - $gitCommitMessage"

                            # no need for a git pull before push
                            # because this is a script, we cannot manually check and resolve any merge conflicts
                            # if the push fails, we just let it fail and have the next run pick it up

                            Write-Output "`STEP 8: Pushing files to Bitbucket repository..."
                            git push origin $project.branchToMergeInto
                            cd ..
                        }
                    }
                }
                else 
                {
                    Write-Output "NO SUBCOMPONENTS"

                    # this is the folder containing the translation files we downloaded from Lokalise
                    $sourceFolder = "$lokaliseDownloadedTranslationsFolder\$($project.name)\$($component.name)"

                    # this is the folder in the bitbucket repository where we will move the downloaded translation files to
                    $destinationFolder = "$($project.translationsRepository)\$($component.pathToSaveTo)"

                    # TODO: consider moving to a function
                    # when download Android and IOS translations in Lokalise, the LANG ISO file uses a hyphen (-) instead of an underscore (_)
                    # we need to change this to an underscore so that it matches the filenames we use in the Mobile Android and IOS components
                    if(($component.name -eq "Mobile Android") -or ($component.name -eq "Mobile IOS"))
                    {
                        Write-Output "`STEP 4.1: Mobile Android/IOS Special Step - Replacing hyphens (-) in the filename with underscores (_)..."

                        $translationFiles = dir $sourceFolder -r  | % { if (-Not $_.PsIsContainer) { $_.FullName }}
                        foreach($translationFile in $translationFiles)
                        {
                            $translationFileNameWithUnderscore = Split-Path $translationFile -leaf
                            $translationFileNameWithUnderscore = $translationFileNameWithUnderscore.replace("-", "_")

                            # for Android XML translations, the LANG_ISO gets appended with an 'r'
                            # instead of ar_SA, it's ar_rSA
                            # what we're trying to do here is we need to dynamically remove r from _r
                            if($component.name -eq "Mobile Android")
                            {
                                if($translationFileNameWithUnderscore.Contains("_r"))
                                {
                                    Write-Output "`STEP 4.2: Mobile Android Special Step - Replacing '_r' with '_' for $translationFileNameWithUnderscore"
                                    $translationFileNameWithUnderscore = $translationFileNameWithUnderscore.replace("_r", "_")
                                }
                            }

                            if($component.name -eq "Mobile IOS")
                            {
                                if($translationFileNameWithUnderscore.Contains("zh_Hans"))
                                {
                                    Write-Output "`STEP 4.2: Mobile IOS Special Step - Replacing 'zh_Hans' with 'zh_CN' for $translationFileNameWithUnderscore"
                                    $translationFileNameWithUnderscore = $translationFileNameWithUnderscore.replace("zh_Hans", "zh_CN")
                                }
                            }

                            Rename-Item -Path $translationFile -NewName $translationFileNameWithUnderscore
                        }
                    }

                    # TODO: move this to a function
                    # when downloaded, the base file ("en" in this case) for resx files in Lokalise do not include the LANG_ISO in the file name
                    # for example, this means that SharedResource.en.resx becomes SharedResource.resx
                    # to fix this, we need to rename the base file and put in the LANG_ISO (en) manually
                    if(($component.translationFileFormat -eq "resx") -And ($component.name -ne "Opri Consumer"))
                    {
                        Write-Output "`STEP 4.1: Project with '$($component.translationFileFormat)' file format detected, generating new filename with base LANG_ISO '$($component.baseLangIso)''"

                        $translationFiles = dir $sourceFolder -r  | % { if (-Not $_.PsIsContainer) { $_.FullName }}
                        foreach($translationFile in $translationFiles)
                        {
                            # CASE 1: file with LANG_ISO WITH culture
                            # e.g. if $splitFullNameArray is D:\a\1\s\lokalise-translations\Opri\Resources.en_AU.resx, it becomes ["D:\a\1\s\lokalise-translations\Opri\Resources", "en_AU", "resx"]

                            # CASE 2: file with LANG_ISO WITHOUT culture
                            # e.g. if $splitFullNameArray is D:\a\1\s\lokalise-translations\Opri\Resources.de.resx, it becomes ["D:\a\1\s\lokalise-translations\Opri\Resources", "de", "resx"]

                            # CASE 3: file without LANG_ISO
                            # e.g. if $splitFullNameArray is D:\a\1\s\lokalise-translations\Opri\Resources.resx, it becomes ["D:\a\1\s\lokalise-translations\Opri\Resources", "resx"]
                            $splitTranslationFileArray = $translationFile.Split(".")
                            $secondToTheLastItemIndex = $splitTranslationFileArray.Length - 2
                            $potentialLangISOCode = $splitTranslationFileArray[$secondToTheLastItemIndex]

                            # we then check if the second to the last item in the array contains an underscore "_"
                            # if it does, then it is is the LANG_ISO and we move to the next item in the list
                            # if it it does not, it can either be another LANG_ISO WITHOUT a culture (lt, lv, fr) OR the name before the LANG_ISO (Resources, SharedResources, etc.)
                            # for BidOne, because we always use a 2 character LANG_ISO code, we determine if it is the file that needs to be renamed based on this (if in the future, we support 3 character LANG_ISO codes, this logic needs to be changed)
                            # if the character count is greater than 2, we need to append the base LANG_ISO (en) LANG_ISO to the file name (insert it as the second last item in the array and combine them all again into one string)
                            if ((-Not $potentialLangISOCode.Contains("_")) -And ($potentialLangISOCode.Length -gt 2)) 
                            {
                                Write-Output "`STEP 4.2: Updating '$translationFile' filename to include base LANG_ISO '$($component.baseLangIso)'"

                                # construct a new array using the data of the original array
                                $newTranslationFileArray = @()
                                for($i = 0; $i -lt $splitTranslationFileArray.Length; $i++)
                                {
                                    # if the current iteration is the last index, append the base LANG_ISO (en) first as the second to the last item in the array before the last item
                                    if($i -eq $splitTranslationFileArray.Length - 1)
                                    {
                                        $newTranslationFileArray += "en"
                                    }
                                    $newTranslationFileArray += $splitTranslationFileArray[$i]
                                }

                                # so based on the example above, for CASE 3, it now becomes ["D:\a\1\s\lokalise-translations\Opri\Resources", "en", "resx"]
                                # we then rename the file with the newly added base LANG_ISO
                                $translationFileWithBaseLangIso = $newTranslationFileArray -join '.'
                                Rename-Item -Path $translationFile -NewName $translationFileWithBaseLangIso
                            }
                        }       
                    }

                    # TODO: consider making this dynamic and moving to a function
                    # Identity Server has a zh LANG_ISO base language file but Lokalise does not support it
                    # to circumvent this, we use zh_CN to translate and rename the downloaded file into zh
                    if($component.name -eq "Identity Server")
                    {
                        Write-Output "`nSTEP 4.3: Identity Server Special Step - Renaming Controllers.AccountController.zh_CN.resx to Controllers.AccountController.zh.resx..."

                        $identityServerFullFileName = Get-ChildItem $sourceFolder -Filter "Controllers.AccountController.zh_CN.$($component.fileExtension)" -Recurse | % { $_.FullName }
                        Rename-Item -Path $identityServerFullFileName -NewName $identityServerFullFileName.replace("zh_CN", "zh")
                    }

                    Write-Output "`nSTEP 5: Moving the below translation files from '$sourceFolder' to '$destinationFolder'..."
                    $translationFiles = dir $sourceFolder -r  | % { if (-Not $_.PsIsContainer) { $_.Name }}
                    Write-Output $translationFiles
                    Copy-Item -Path "$($sourceFolder)\*.$($component.fileExtension)" -Destination $destinationFolder -Recurse

                    if($releaseMode)
                    {
                        Write-Output "`nSTEP 6: Committing $($component.name) translation files to local repository..."
                        cd $project.translationsRepository
                        git add --all
                        git commit -m "AZDEVOPS - $($component.name) - $gitCommitMessage"

                        # no need for a git pull before push
                        # because this is a script, we cannot manually check and resolve any merge conflicts
                        # if the push fails, we just let it fail and have the next run pick it up

                        Write-Output "`nSTEP 7: Pushing files to Bitbucket repository..."
                        git push origin $project.branchToMergeInto
                        cd ..
                    }
                }
                
                Write-Output "************************************************************`n"
                Write-Output "-- COMPONENT '$($component.name)' END --`n"
                Write-Output "************************************************************`n"
            }
        }

        Write-Output "##############################################################################################################################`n`n"
        Write-Output "-- PROJECT '$($project.name)' END --`n`n"
        Write-Output "##############################################################################################################################`n`n"
    }
}
