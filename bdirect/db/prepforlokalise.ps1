#This script is used rename the props from transifex propname to lokalise
$files = Get-ChildItem ".\" -Recurse | Where-Object {$_.FullName.EndsWith(".json")}

foreach ($file in $files) {
  replace $file.FullName
}
# the original json text

function replace ($fileName){
Write-Host $fileName
$json = Get-Content $fileName

$json = $json.Replace('"translation"', '"translation"').Replace('"limit"', '"limit"') | Out-File $fileName

} 
