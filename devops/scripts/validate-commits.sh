#!/bin/bash   


############################################################
# Script: Global Variables                                 #
############################################################

component_name=""                         # Component Name                E.g. AdminV5, BDirect Web, etc..
commit_period="4 hours ago"               # Commit Period                 E.g. Git commit period. Using this it will be constructed as `4 hours ago`.
ignore_commit_check=false                 # Ignore Commit Check           Default value is false

############################################################
# Function: Help                                           # 
############################################################
usage() {
   # Display Help
  echo
  echo "BDirect Translations Commits"
  echo
  echo "Available options"
  echo 
  echo "h     Print this Help."
  echo "check -c <component_name> -p <Commit period> -disable <Boolean flag to ignore periodic commit check>      # Chech Repo Commits"  
  exit 1
}

############################################################
# Function: Check Commits                      
############################################################
checkCommits() {

   # Parse options for the 'rename' function
    while getopts ":c:p:d:h" opt; do
      case $opt in
        h)
          usage
          ;;
        c)
          component_name="$OPTARG"
          ;;
        p)
          commit_period="$OPTARG"
          ;;   
        d)
          ignore_commit_check="$OPTARG"
          ;;          
        \?)
          echo "Invalid option: -$OPTARG" >&2
          ;;
        :)
          echo "Option -$OPTARG requires an argument." >&2
          usage
          ;;
      esac
    done

    # Check if required options are provided
    if [ -z "$component_name" ]; then
      echo -e "\e[31m Error:  Component Name is mandatory.\e[0m"
      usage
    fi

    set -e

    # Git fetch
    #git pull

    echo  -e "\e[32m Commit Period: $commit_period \e[0m"
    echo  -e "\e[32m Ignore Commit Check: $ignore_commit_check \e[0m"

    # Get the latest commit
    if [ "$ignore_commit_check" = "True" ]; then
      latest_commit=$(git log --grep="$component_name"  -n 1 --pretty=format:'%h')
    else
      latest_commit=$(git log --since="$commit_period" --grep="$component_name"  -n 1 --pretty=format:'%h')
    fi
    run_deploy=false

    if [ -n "$latest_commit" ]; then
    
        echo  -e "\e[32m Latest commit for $component_name: $latest_commit \e[0m"

        run_deploy=true   

        echo -e "\e[32m Run Deployment For $component_name \e[0m"
    else
        echo -e "\e[33m No commits found.\e[0m"
    fi

    # Set an environment variable for the latest commit
    echo "##vso[task.setvariable variable=LatestCommit;isOutput=true]$latest_commit"
    echo "##vso[task.setvariable variable=CanRun;isOutput=true]$run_deploy"
}

############################################################
# Script: Globals                        
############################################################

# Check if at least one argument is provided
if [ $# -lt 1 ]; then
  echo 
  echo -e "\e[31mError: Function name is missing.\e[0m" 
  echo
  usage
fi

# Extract the function name and remove it from the argument list
function_name="$1"
shift


# Call the appropriate function based on the provided function name
case "$function_name" in
  "check")
    checkCommits "$@"
    ;;
    *)
    echo "Error: Invalid function name: $function_name"
    usage
    ;;
esac