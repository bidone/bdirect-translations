#!/bin/bash


############################################################
# Script: Global Variables                                 #
############################################################

src_directory=""                # Source Directory              E.g. $(Build.SourcesDirectory)/bidone-translations/src/adminv5/webapp
dest_directory=""               # Destination Directory         E.g. $(Build.SourcesDirectory)/bidone-translations/src/adminv5/webapp
sites_codes=""                  # Site Cultures JSON File Path. E.g. $(Build.SourcesDirectory)/bdirect-automation/azure-resources/devops/variables/siteCultures.json




############################################################
# Function: Help                                           # 
############################################################
usage() {
   # Display Help
  echo
  echo "Bidone Translations Script"
  echo
  echo "Available options"
  echo 
  echo "h     Print this Help."
  echo " rename -s <src_directory> -d <src_directory>.      # Rename translations files. E.g en_US to en-US"
  echo " copy -s <src_directory> -d <src_directory> -sc <site_codes>. # Copy translations from source to destination file path for each given site."
  exit 1
}


############################################################
# Function: Rename Translation Files 
# E.g en_US to en-US                            
############################################################
rename() {

   # Parse options for the 'rename' function
    while getopts ":s:d:h" opt; do
      case $opt in
        h)
          usage
          ;;
        s)
          src_directory="$OPTARG"
          ;;
        d)
          dest_directory="$OPTARG"
          ;;   
        \?)
          echo "Invalid option: -$OPTARG" >&2
          ;;
        :)
          echo "Option -$OPTARG requires an argument." >&2
          usage
          ;;
      esac
    done

    # Check if required options are provided
    if [ -z "$src_directory" ] || [ -z "$dest_directory" ]; then
      echo -e "\e[32m Error:  Source File path, desination file paths are mandatory.\e[0m"
      usage
    fi

    # Move to working directory

    echo 
    echo -e "File Working Directory: \e[32m $src_directory \e[0m"
    cd $src_directory
    
    # Iterate through each file in the folder
    for file in *.json; do
    # Check if the file contains an underscore
    if [[ "$file" == *_* ]]; then
    
    # Replace underscores with hyphens in the filename
    new_filename=$(echo "$file" | sed 's/_/-/g')
        
        # Copy and rename the file
        cp "$file" "$new_filename"
    fi
    done

    # Delete files with underscores in their names
    find . -type f -name '*_*' -delete
    
    # List the files to verify the renaming
    echo 
    echo -e "\e[32m Renamed Files \e[0m" 
    echo 
    ls -R $dest_directory
}

##############################################################################
# Function: Copy all json files from source and it's subdirectories to destination folder
#           file path for each given site.                   
##############################################################################
copyJSONFilesToFlatDirectory(){
     # Parse options for the 'rename' function
    while getopts ":s:d:h" opt; do
      case $opt in
        h)
          usage
          ;;
        s)
          src_directory="$OPTARG"
          ;;
        d)
          dest_directory="$OPTARG"
          ;;   
        \?)
          echo "Invalid option: -$OPTARG" >&2
          ;;
        :)
          echo "Option -$OPTARG requires an argument." >&2
          usage
          ;;
      esac
    done

    # Check if required options are provided
    if [ -z "$src_directory" ] || [ -z "$dest_directory" ]; then
      echo -e "\e[32m Error:  Source File path, desination file paths are mandatory.\e[0m"
      usage
    fi

    # Linux is case sensitive. Setting source path to lowercase
    source_path=$(echo "$src_directory" | tr '[:upper:]' '[:lower:]')

    # Move to working directory

    echo 
    echo -e "File Working Directory: \e[32m $source_path \e[0m"
    cd $source_path
    

    # Create the destination directory if it doesn't exist
    mkdir -p "$dest_directory"

    # Use find to locate all JSON files in the source directory and its subdirectories
    find "$source_path" -type f -name "*.json" -exec cp {} "$dest_directory" \;

    # Print a message indicating the completion of the copy operation
    echo "All JSON files from $source_path and its subdirectories have been copied to $dest_directory."
    
    # List the files to verify the renaming
    echo 
    echo -e "\e[32m Renamed Files \e[0m" 
    echo 
    ls -R $dest_directory
}



##############################################################################
# Function: Copy translations from source to destination 
#           file path for each given site.                   
##############################################################################
copy(){

    # Parse options for the 'copy' function
    while getopts ":s:d:c:h" opt; do
      case $opt in
        h)
          usage
          ;;
        s)
          src_directory="$OPTARG"
          ;;
        d)
          dest_directory="$OPTARG"
          ;;
        c)
          site_codes="$OPTARG"
          ;;        
        \?)
          echo "Invalid option: -$OPTARG" >&2
          ;;
        :)
          echo "Option -$OPTARG requires an argument." >&2
          usage
          ;;
      esac
    done

    # Check if required options are provided
    if [ -z "$src_directory" ] || [ -z "$dest_directory" ] || [ -z "$site_codes" ]; then
      echo
      echo  -e "\e[31m Error: Source File path, desination file path and site codes are mandatory \e[0m" 
      echo
      usage
    fi    

    # Get JSON Content
    jsonContent=$(cat $site_codes | tr -cd '\11\12\15\40-\176')

    # Iterate Site Codes JSON array using `jq`.
    # https://jqlang.github.io/jq/

    for row in $(echo "${jsonContent}" | jq -r '.[] | @base64'); do
        _jq() {
            echo ${row} | base64 --decode | jq -r ${1}
        }

        #Local Varaible
        site=$(_jq '.siteCode')

        # Check if the 'site' is not empty
        if [ -n "$site" ]; then

            # Perform copy operation for the given site
            
            echo 
            echo -e "\e[32m Populate Translation For Site: $site \e[0m"
            echo 

            echo 
            echo -e "\e[32m Working Directory \e[0m"
            echo 

            ls -R $src_directory

            # Convert Site Code to Lower Case. 
            # This conversion required due to web config settings
            site_code=$(echo "$site" | tr '[:upper:]' '[:lower:]')

            # Create the destination folder if it doesn't exist
            mkdir -p "$dest_directory/$site_code"

            # Copy site specific extracted translation content to repo src directory
            cd $src_directory
            cp -R *.json "$dest_directory/$site_code"

            # Print translated files
            echo 
            echo -e "\e[32m Translated Files \e[0m"
            echo 

            # List Availables for the given directory
            ls -R "$dest_directory/$site_code"

        else
            # Print message when the site is empty
            echo -e "\e[33m Warning: Site is empty. Skipping...\e[0m" 
        fi
    done   
}


############################################################
# Function: Set Script Arguments                             
############################################################
setArguments(){
      # Parse options for the 'rename' function
    while getopts ":h:s:d:sc:" opt; do
      case $opt in
        h)
          usage
          ;;
        s)
          src_directory="$OPTARG"
          ;;
        d)
          dest_directory="$OPTARG"
          ;;
        sc)
          sites_codes="$OPTARG"
          ;;        
        \?)
          echo "Invalid option: -$OPTARG" >&2
          ;;
        :)
          echo "Option -$OPTARG requires an argument." >&2
          usage
          ;;
      esac
    done
}


############################################################
# Script: Globals                        
############################################################

# Check if at least one argument is provided
if [ $# -lt 1 ]; then
  echo 
  echo -e "\e[31mError: Function name is missing.\e[0m" 
  echo
  usage
fi

# Extract the function name and remove it from the argument list
function_name="$1"
shift


# Call the appropriate function based on the provided function name
case "$function_name" in
  "copy")
    copy "$@"
    ;;
  "rename")
    rename "$@"
    ;;
  "copyJSONFilesToFlatDirectory")
    copyJSONFilesToFlatDirectory "$@"
    ;;
  *)
    echo "Error: Invalid function name: $function_name"
    usage
    ;;
esac
