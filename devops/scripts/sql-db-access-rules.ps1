Param(
    [string]$locationPrefix,
    [string]$sqlDbEnv,
    [string]$sqlNamePrefix,
    [bool]$allowAccess,
    [int]$nsgRulePriority
)

# Apppend character to location prefix.
if ($locationPrefix -ne ""){
    $locationPrefix = "$locationPrefix-"
}

if($sqlDbEnv -eq "preprod"){
    $sqlDbEnv = "uat" # Replace preprod with uat, as preprod is not a valid environment for SQL NSG
}

# Resource Group Name
$rg = "$($locationPrefix)bd-$($sqlDbEnv)-rg"

# Network Security Group Name
if(($locationPrefix -eq "weu-") -and ($sqlDbEnv -eq "uat")){
    $nsgName = "$($locationPrefix)bd-2-$($sqlNamePrefix)$($sqlDbEnv)-sql-nsg" # Custom Name Configured WEU UAT NSG. Hence, this condition required
}else{
$nsgName = "$($locationPrefix)bd-$($sqlNamePrefix)$($sqlDbEnv)-sql-nsg"
}
# Agent IP Address
$HostedIPAddress = Invoke-RestMethod https://ipinfo.io/json | Select -exp ip
# NSG Rule Priority
$rulePriority = $nsgRulePriority 
# NSG Rule Name
$nsgRuleName = "deploy_translations_" + $HostedIPAddress + $nsgRulePriority


# Method to add rules in Azure Sql VNET
function addRules() {

    Write-Host
    Write-Host "Adding agent IP to SQL vnet..."
    Write-Host

    $ruleExists = (az network nsg rule show -g $rg --nsg-name $nsgName -n $nsgRuleName)

    if ($ruleExists) {
        Write-Host "$nsgRuleName nsg role already exists"
    }
    else {
        $nsgRuleName =  (az network nsg rule create -g $rg --nsg-name $nsgName -n $nsgRuleName `
                        --protocol Tcp --access Allow  --priority $rulePriority `
                        --source-port-ranges '*' --source-address-prefixes $HostedIPAddress `
                        --destination-port-ranges 3342 --destination-address-prefixes '*' `
                        --query "name" --output tsv)

        Write-Host
        Write-Host "$nsgRuleName nsg rule created successfully"
        Write-Host
    }
}

# Method to remove rules from Azure Sql VNET
function removeRules() {
    Write-Host
    Write-Host "Removing agent IP to SQL vnet..."
    Write-Host

    $ruleExists = az network nsg rule show -g $rg --nsg-name $nsgName -n $nsgRuleName

    if ($ruleExists) {
        az network nsg rule delete -g $rg --nsg-name $nsgName -n $nsgRuleName

        Write-Host
        Write-Host "$nsgRuleName nsg rule removed successfully"
        Write-Host
    }
    else {
        Write-Host "$nsgRuleName nsg rule not found"
    }
}

# Main Script
if($allowAccess -eq $true){
    addRules
}else{
    removeRules
}

