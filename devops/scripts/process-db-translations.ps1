[CmdletBinding()]
param(
    [string]$buildNumber,
    [string]$sqlServer,
    [string]$sqlDbEnv,
    [string[]]$sites,
    [string]$dbComponent,
    [string]$dbPwd,
    [string]$locationPrefix,
    [string]$locationPrefixPrimary,
    [string]$sqlNamePrefix,
    [string]$blobPrefix,
    [string]$translationProcName
)

# Apppend character to location prefix.
if ($locationPrefix -ne ""){
    $locationPrefix = "$locationPrefix-"
}

if ($locationPrefixPrimary -ne ""){
    $locationPrefixPrimary = "$locationPrefixPrimary-"
}

Write-Host
Write-Host "Running Db Translations..." $siteCode -ForegroundColor Green
Write-Host

# Print Cloud Regions
Write-Host "Cloud Region" -ForegroundColor Green
Write-Host "--------------"
Write-Host $locationPrefix
Write-Host $locationPrefixPrimary

# Print Environment
Write-Host "Environment" -ForegroundColor Green
Write-Host "--------------"
Write-Host $sqlDbEnv

# Print Available sites 
Write-Host "Available Sites" -ForegroundColor Green
Write-Host "---------------"
Write-Host $sites


# Set `$sqlServer` variable to SQL Server public endpoint
[regex]$hostMatch = "\."
$sqlServer = $hostMatch.replace($sqlServer, ".public.", 1)  + ",3342"
$sqlServer = $sqlServer.replace("-failover", "")
$sqlServer = $sqlServer.replace("bd-", "bd-" + $sqlNamePrefix)

if ($locationPrefix -and ($locationPrefix -ne $locationPrefixPrimary))
{
    [regex]$hostMatchPrefix = $locationPrefixPrimary
    $sqlServer = $hostMatchPrefix.replace($sqlServer, $locationPrefix, 1)
}

# Custom SQL Server Setup For WEU UAT
if(($locationPrefix -eq "weu-") -and ($sqlDbEnv -eq "uat" -or $sqlDbEnv -eq "preprod")){
    $sqlServer = "weu-bd-2-uat-sql.public.06f602e414b9.database.windows.net,3342" # Custom Name Configured For WEU UAT SQL Server. Hence, this patch work required
}

# Print Available sites 
Write-Host "Targeted SQL Server" -ForegroundColor Green
Write-Host "-------------------"
Write-Host $sqlServer

# Process DB Translations for each site
forEach ($siteCode in $sites){

    # Local Variables
    if($sqlDbEnv -eq "preprod"){
        $dbUserName = "$($locationPrefixPrimary)bd-uat-sql-admin"
    }
    else{
        $dbUserName = "$($locationPrefixPrimary)bd-$($sqlDbEnv)-sql-admin"
    }
    #$blobStoragePath = "translations/$($dbComponent)/BDirect DB $($buildNumber)/"
    $blobStoragePath = "translations/$($blobPrefix)/"
    $dbName = "Initial Catalog=$($siteCode).$($sqlDbEnv).$($dbComponent);"
    $dbCredentials = "User ID=$($dbUserName);Password=$($dbPwd);"
    $sqlDbConnectRetryCount = 3
    $sqlDbConnectRetryInterval = 30
    $sqlDbConnectionTimeout = 120
    $sqlAuthenticationProvider = ''
    $sqlDbConnStringSegmentShared = "Server=tcp:$($sqlServer);Persist Security Info=False;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;ConnectRetryCount=$($sqlDbConnectRetryCount);ConnectRetryInterval=$($sqlDbConnectRetryInterval);Connection Timeout=$($sqlDbConnectionTimeout);$($sqlAuthenticationProvider)"

    # DB Connection String
    $sqlDbConnString = "$($dbName)$($dbCredentials)$($sqlDbConnStringSegmentShared)"	

    Write-Host    
    Write-Host  "---------------------------------------------" -ForegroundColor Yellow
    Write-Host

    Write-Host "Processing Db Translations For Site: $siteCode"  -ForegroundColor Green
    Write-Host "Db Name: $($siteCode).$($sqlDbEnv).$($dbComponent)"  -ForegroundColor Green
    Write-Host

    Write-Host "Executing Db Translations..."

    # Stored Proc Name to execute
    # $translationProcName = "[Core].[ProcessTranslations]"

    # Sql Query to execute translations
    $query = "EXEC $($translationProcName) "+ "'$blobStoragePath'"
    $queryWithIfBlock = "IF(SELECT COUNT(*) FROM sys.all_objects WHERE TYPE = 'P' and NAME = 'ProcessTranslations') = 1 BEGIN "+$query+" END"

    #Write-Host $queryWithIfBlock

    # Execute SQL query
    Invoke-Sqlcmd -ConnectionString $($sqlDbConnString) -Query $queryWithIfBlock

    Write-Host "Db Translations For Site $siteCode Complete" -ForegroundColor Yellow
}

Write-Host
Write-Host "Done" -ForegroundColor Green
Write-Host