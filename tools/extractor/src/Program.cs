﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace TranslationsExtractor
{
    class Program
    {
        static void Main(string[] args)
        {
            //Compare(@"C:\@Projects\Bidone\bidvestdirect_shop_frontend\Calypso.Web.UI\BShop\app\languages\en.json", @"C:\Users\pavan.josyula\Downloads\en-AU.json");
            //Console.ReadLine();

            string sourcePath = string.Empty;
            string destinationPath = string.Empty;

            string siteCultureMap = string.Empty;
            string subFolderPath = "//";
             string menuPlanningPath = string.Empty;
            bool generate = false;
            IEnumerable<string> siteCodes = Enumerable.Empty<string>();
            foreach(var arg in args)
            {
                var items = arg.Split('=');
                if (items[0] == "-g")
                {
                    generate = items[0] == "-g";
                }
                else if(items[0] == "-si")
                {
                    siteCultureMap = items[1];
                }
                else if (items[0] == "-s")
                {
                    sourcePath = items[1];
                }
                else if (items[0] == "-d")
                {
                    destinationPath = items[1];
                }
                else if (items[0] == "-site")
                {
                    siteCodes = items[1].Split(',');
                }
                else if (items[0] == "-dsub")
                {
                    subFolderPath = items[1];
                }
                else if (items[0] == "-mps")
                {
                    menuPlanningPath = items[1];
                }
            }

            IEnumerable<SiteToCulture> siteToCultures = JsonConvert.DeserializeObject<IEnumerable<SiteToCulture>>(siteCultureMap);

            //This is only required when extracting and merging site specific JSON overrides and combining them as one.
            if (generate)
            {
                Generator.GenerateBaseLanguageFiles(@"C:\@Projects\Bidone\bidvestdirect_shop_frontend\Calypso.Web.UI\BShop\app\languages\"
                                                    , @"C:\@Projects\Bidone\bidvestdirect_configuration\Frontend\"
                                                    , @"C:\Temp\shopv5\",
                                                    siteToCultures);
                Console.WriteLine("Generated");
            }

            //This is to extract site specific json files
            Extractor.ExtractSiteTranslations(siteToCultures, sourcePath, destinationPath, siteCodes, subFolderPath, menuPlanningPath);
            Console.WriteLine("Extracted");
        }
        static void Compare(string filePath1, string filePath2)
        {
            var source = Util.LoadAsJObject(Util.LoadFile(filePath1));
            var result = Util.LoadAsJObject(Util.LoadFile(filePath2));

            foreach(var prop in source.Properties())
            {
                var item = result.Properties().FirstOrDefault(i => i.Name == prop.Name);
                if(item == null)
                {
                    Util.WriteToConsole($"Missing in result {prop.Name}");
                }
            }
        }

       

    }

   
}
