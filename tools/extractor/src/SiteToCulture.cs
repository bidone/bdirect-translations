﻿using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace TranslationsExtractor
{
    public class SiteToCulture
    {
        [JsonProperty("siteCode")]
        public string SiteCode { get; set; }

        [JsonProperty("cultureCodes")]
        public IEnumerable<string> CultureCodes { get; set; }

        private IEnumerable<LanguageCulture> _LanguageCultures;

        public IEnumerable<LanguageCulture> LanguageCultures
        {
            get
            {
                if(_LanguageCultures != null && _LanguageCultures.Any())
                {
                    return _LanguageCultures;
                }

                _LanguageCultures = CultureCodes.Select(i =>
                {
                    var items = i.Split("_");
                    var l = items[0];
                    var c = items.Length > 1 ? items[1] : string.Empty;
                    return new LanguageCulture() { CultureCode = c, IsoCulture = i, LanguageCode = l };
                });

                return _LanguageCultures;
            }

        }

        public IEnumerable<string> LangCodes => LanguageCultures.Select(i => i.LanguageCode);

    }

    public class LanguageCulture
    {
        public string LanguageCode { get; set; }
        public string CultureCode { get; set; }
        public string IsoCulture { get; set; }
    }
}
