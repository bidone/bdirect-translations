﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace TranslationsExtractor
{
    public class Generator
    {
        public static void GenerateBaseLanguageFiles(string baseFolderPath, string siteOverrideFilePath, string outputFolderPath,  IEnumerable<SiteToCulture> siteToCultures)
        {
            var langCodes = siteToCultures.SelectMany(i => i.CultureCodes).Select(i => i.Split('-')[0]).Distinct();


            foreach (var siteCulture in siteToCultures.SelectMany(i => i.CultureCodes).Distinct())
            {
                var langCode = siteCulture.Split('-')[0];
                //string baseFilePath = string.Empty;
                string baseContents = string.Empty;

                if (langCode.Contains("_"))
                {
                    langCode = langCode.Split('_')[0];
                }
                else if (langCode.Contains("-"))
                {
                    langCode = langCode.Split('-')[0];
                }
                var baseFilePath = @$"{baseFolderPath}{langCode}.json";
                baseContents = Util.LoadFile(baseFilePath);
                if (string.IsNullOrWhiteSpace(baseContents))
                {
                    return;
                }

                JObject baseObject = Util.LoadAsJObject(baseContents);
                string finalString = string.Empty;


                foreach (var siteCode in siteToCultures.Where(i => i.CultureCodes.Contains(siteCulture)).Select(i => i.SiteCode).ToList())
                {
                    //if (!siteToCultures.Any(i => i.CultureCodes.Any(i => i.Split('-')[0] == langCode)))
                    //{
                    //    continue;
                    //}
                    string siteSpecificPath = @$"{siteOverrideFilePath}{siteCode}\Languages\{langCode}.json";
                    if (!System.IO.File.Exists(siteSpecificPath))
                    {
                        finalString = baseObject.ToString();
                        continue;
                    }
                    string sitespecificContents = Util.LoadFile(siteSpecificPath);
                    JObject siteObject = Util.LoadAsJObject(sitespecificContents);
                    baseObject = AppendSiteCodeToKey(siteObject, baseObject, siteCode, langCode == "en");
                    // finalString = Merge(baseObject, siteObject);
                }
                Directory.CreateDirectory($@"{outputFolderPath}");

                string pathToWrite = @$"{outputFolderPath}{siteCulture}.json";

                System.IO.File.WriteAllText(@$"{outputFolderPath}{siteCulture}.json", baseObject.ToString());
            }
        }
     
        static JObject AppendSiteCodeToKey(JObject siteObject, JObject baseObject, string siteCodeToAppend, bool appendSiteCode)
        {
            var props = siteObject.Properties().ToList();

            foreach (var p in props)
            {
                var baseValue = baseObject[p.Name];
                var siteValue = siteObject[p.Name];
                if (baseValue == null)
                {
                    baseObject.Add($"{p.Name}", p.Value);
                    continue;
                }
                else if (baseValue != null && siteValue != null && baseValue.ToString().Trim() != siteValue.ToString().Trim())
                {
                    if (appendSiteCode)
                    {
                        baseObject.Add($"{siteCodeToAppend}_{p.Name}", p.Value);
                        continue;
                    }
                    else
                    {
                        baseObject[p.Name] = p.Value;
                    }
                }

            }

            return baseObject;
        }
    }
}
