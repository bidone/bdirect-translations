﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Text;

namespace TranslationsExtractor
{
    public class StringComparer : IEqualityComparer<string>
    {
        public bool Equals([AllowNull] string x, [AllowNull] string y)
        {
            return x?.ToLower() == y?.ToLower();
        }

        public int GetHashCode([DisallowNull] string obj)
        {
            if (obj == null)
                return 0;
            else
                return obj.ToLower().GetHashCode();
        }
    }

    public enum ExtractFromCsvType
    {
        JsonKeyValue,
        AndroidXml,
        iOS
    }

    public enum ExtractSeperator
    {
        Comma,
        Tab

    }

    public class Extractor
    {
        public static void ExtractSiteTranslations(IEnumerable<SiteToCulture> siteToCultures, 
            string sourcePath, 
            string destinationPath, 
            IEnumerable<string> siteCodes, 
            string subFolder, 
            string menuPlanningPath)
        {
            IEnumerable<SiteToCulture> chosenSites = siteToCultures;
            if (siteCodes.Any())
            {
                chosenSites = siteToCultures.Where(i => siteCodes.Contains(i.SiteCode, new StringComparer()));
            }

            foreach (var site in chosenSites)
            {
                var culturesToLoops = site.CultureCodes.ToList();

                var otherCultures = siteToCultures.SelectMany(i => i.LanguageCultures)
                                                .Select(i => i.IsoCulture).Distinct().ToList();

                culturesToLoops.AddRange(otherCultures.Except(culturesToLoops));
                foreach (var culture in culturesToLoops)
                {
                    var items = culture.Split("_");
                    if (items.Length > 1
                        && site.LanguageCultures.Where(i => i.LanguageCode == items[0] && i.CultureCode != items[1]).Any())
                    {
                        continue;
                    }
                    Util.WriteToConsole($"Working on site-{site.SiteCode} and culture {culture}");
                    string allContents = Util.LoadFile(@$"{sourcePath}{culture}.json");
                    Util.WriteToConsole($"Fetched based contents from {sourcePath}{culture}.json for site- {site.SiteCode} and culture {culture}");

                    if (string.IsNullOrWhiteSpace(allContents))
                    {
                        Util.WriteToConsole("no contents found");
                        continue;
                    }
                    string extractedContent = ExtractSiteRelatedContent(allContents, site.SiteCode, siteToCultures.Select(i => i.SiteCode)).ToString();
                    Util.WriteToConsole($"Extracted contents for site-{site.SiteCode} and culture {culture}");

                    //Merging menu planning content if provided
                    if(!string.IsNullOrEmpty(menuPlanningPath)){
                         string allMenuPlanningContents = Util.LoadFile(@$"{menuPlanningPath}{culture}.json");
                         if(!string.IsNullOrEmpty(allMenuPlanningContents)){
                            extractedContent = extractedContent.ConcatinateLanguageJson(allMenuPlanningContents);
                         }
                    }

                    string path = @$"{destinationPath}{site.SiteCode.ToLower()}{subFolder}";
                    Directory.CreateDirectory(path);
                    Util.WriteToConsole($"Created Directory if not exists {path} for site-{site.SiteCode} and culture {culture}");

                    string pathToWrite = @$"{path}{culture.Split('_')[0]}.json";
                    System.IO.File.WriteAllText(pathToWrite, extractedContent);

                    Util.WriteToConsole($@"File is available at {pathToWrite} for site-{site.SiteCode} and culture {culture}");

                }
            }
        }

        public static JObject ExtractSiteRelatedContent(string finalString, string siteCode, IEnumerable<string> allSiteCodes)
        {
            JObject obj = Newtonsoft.Json.JsonConvert.DeserializeObject<JObject>(finalString);

            JObject siteObject = new JObject();
            var lowerCaseSiteCodes = allSiteCodes.Select(i => $"{i.ToLower()}");
            string appendedSiteCode = $"{siteCode}";
            foreach (var p in obj.Properties().Where(i => i.Name.StartsWith(appendedSiteCode)))
            {
                var key = p.Name.ReplaceFirst($"{appendedSiteCode}_", "");
                if (!siteObject.ContainsKey(key))
                {
                    siteObject.Add(key, p.Value.ToString()=="*"?"":p.Value);
                }
            }

            var baseprops = obj.Properties().Where(i =>
            {
                var name = $"{i.Name.Split('_')[0].ToLower()}";
                return !lowerCaseSiteCodes.Contains(name.ToLower());
            }).ToList();

            foreach (var p in baseprops)
            {
                if (siteObject.ContainsKey(p.Name))
                {
                    continue;
                }
                if (p.Value.ToString() == "*")
                {
                    siteObject.Add(p.Name, "");
                }
                else
                {
                    siteObject.Add(p.Name, p.Value);
                }
            }

            return siteObject;
        }

        public static void ExtractSiteTranslationsFromCsv(string sourcePath, string destinationPath, string columnNameToExtract, ExtractFromCsvType typeOfOutput, ExtractSeperator separator, int tagColumnIndex = 1)
        {
            string[] contents = File.ReadAllLines(sourcePath);
            string sepChar = separator == ExtractSeperator.Comma ? "," : "\t";

            int index = Array.IndexOf(contents[0].Split(sepChar), columnNameToExtract);

            if (typeOfOutput == ExtractFromCsvType.JsonKeyValue)
            {
                var finalString = ExtractAsJsonKeyvalue(tagColumnIndex, contents, index, sepChar);
                File.WriteAllText(destinationPath, finalString);
            }
            else if (typeOfOutput == ExtractFromCsvType.AndroidXml)
            {
                var finalString = ExtractAsAndroidXml(tagColumnIndex, contents, index, sepChar);
                File.WriteAllText(destinationPath, finalString);
            }
            else if (typeOfOutput == ExtractFromCsvType.iOS)
            {
                var finalString = ExtractAsiOS(tagColumnIndex, contents, index, sepChar);
                File.WriteAllText(destinationPath, finalString, Encoding.Unicode);
            }
        }

        private static string ExtractAsJsonKeyvalue(int tagColumnIndex, string[] contents, int index, string separator = ",")
        {
            JObject jObject = new JObject();
            foreach (var line in contents.Skip(1))
            {
                var splitLine = line.Split(separator);
                if (splitLine.Length > index)
                {
                    if (!jObject.ContainsKey(splitLine[tagColumnIndex]) && !string.IsNullOrWhiteSpace(splitLine[tagColumnIndex]))
                    {
                        jObject.Add(splitLine[tagColumnIndex], splitLine[index]);
                    }

                }
            }
            string finalString = jObject.ToString();
            return finalString;
        }

        private static string ExtractAsAndroidXml(int tagColumnIndex, string[] contents, int index, string separator = ",")
        {
            StringBuilder elements = new StringBuilder();
            elements.AppendLine("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            elements.AppendLine("<resources xmlns:tools=\"http://schemas.android.com/tools\">");
            foreach (var line in contents.Skip(1))
            {
                var splitLine = line.Split(separator);
                if (splitLine.Length > index && !string.IsNullOrWhiteSpace(splitLine[tagColumnIndex]))
                {
                    elements.AppendLine($"<string name=\"{splitLine[tagColumnIndex]}\">{splitLine[index]}</string>");
                }
            }
            elements.AppendLine("</resources>");
            string finalString = elements.ToString();
            return finalString;
        }

        private static string ExtractAsiOS(int tagColumnIndex, string[] contents, int index, string separator = ",")
        {
            StringBuilder sb = new StringBuilder();
            foreach (var line in contents.Skip(1))
            {
                var splitLine = line.Split(separator);
                if (splitLine.Length > index
                    && !string.IsNullOrWhiteSpace(splitLine[tagColumnIndex])
                    && !string.IsNullOrWhiteSpace(splitLine[index]))
                {
                    sb.AppendLine($"\"{splitLine[tagColumnIndex]}\"=\"{splitLine[index].Trim()}\";");
                }
            }
            string finalString = sb.ToString();
            return finalString;
        }
    }
}
