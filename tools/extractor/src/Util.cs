﻿using System;
using System.IO;
using System.Linq;
using Newtonsoft.Json.Linq;

namespace TranslationsExtractor
{
    public static class StringExtensionMethods
    {
        public static string ReplaceFirst(this string text, string search, string replace)
        {
            int pos = text.IndexOf(search);
            if (pos < 0)
            {
                return text;
            }
            return text.Substring(0, pos) + replace + text.Substring(pos + search.Length);
        }
        public static string ConcatinateLanguageJson(this string json1, string json2)
        {
            json1 = json1.ReplaceFirst("{", "").ReplaceLast("}", "");
            json2 = json2.ReplaceFirst("{", "").ReplaceLast("}", "");

            var comma = !string.IsNullOrEmpty(json1) && !string.IsNullOrEmpty(json2) ? ",\n" : "";

            return "{" + $"{json1}{comma}{json2}" + "}";
        }

        private static string ReplaceLast(this string text, string search, string replace)
        {
            int pos = text.LastIndexOf(search);
            if (pos < 0)
            {
                return text;
            }
            return text.Substring(0, pos) + replace + text.Substring(pos + search.Length);
        }
    }
    public class Util
    {
        public static string LoadFile(string path)
        {

            if (!File.Exists(path))
            {
                Util.WriteToConsole($"{path} not found");
                return string.Empty;
            }
            return System.IO.File.ReadAllText(path);
        }

        public static JObject LoadAsJObject(string contents)
        {
            JObject obj = Newtonsoft.Json.JsonConvert.DeserializeObject<JObject>(contents);
            return obj;
        }

        public static JObject AppendSiteCodeToKey(JObject siteObject, JObject baseObject, string siteCodeToAppend, bool appendSiteCode)
        {
            var props = siteObject.Properties().ToList();

            foreach (var p in props)
            {
                var baseValue = baseObject[p.Name];
                var siteValue = siteObject[p.Name];
                if (baseValue == null)
                {
                    baseObject.Add($"{p.Name}", p.Value);
                    continue;
                }
                else if (baseValue != null && siteValue != null && baseValue.ToString().Trim() != siteValue.ToString().Trim())
                {
                    if (appendSiteCode)
                    {
                        baseObject.Add($"{siteCodeToAppend}_{p.Name}", p.Value);
                        continue;
                    }
                    else
                    {
                        baseObject[p.Name] = p.Value;
                    }
                }

            }

            return baseObject;
        }

        public static void WriteToConsole(string message)
        {
            if(Environment.GetEnvironmentVariable("DEBUGMODE") == "true")
            {
                Console.WriteLine(message);
            }
        }
    }
}
