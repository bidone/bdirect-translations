﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using TranslationsExtractor;
using Xunit;

namespace TranslationExtractor_Tests
{
    public class ExtractorTests
    {
        [Fact]
        public void ExtractTest()
        {

            Thread.Sleep(4000);
            var baseDirectory = $@"{System.AppDomain.CurrentDomain.BaseDirectory}extracts\";
            var outputDirectory = @$"{System.AppDomain.CurrentDomain.BaseDirectory}\outputs";
            var sitesToExtract = new List<string>() { "AU", "FSUK" };
            if (Directory.Exists(outputDirectory))
            {
                Directory.Delete(outputDirectory, true);

            }
            var siteCultureMaps = new List<SiteToCulture>()
            {
                    new SiteToCulture(){
                        CultureCodes = new List<string>{ "en"},
                        SiteCode = "AU"
                    },
                    new SiteToCulture(){
                        CultureCodes = new List<string>{ "en"},
                        SiteCode = "ZA"
                    },
                    new SiteToCulture(){
                        CultureCodes = new List<string>{ "en"},
                        SiteCode = "FSUK"
                    },
                    new SiteToCulture(){
                        CultureCodes = new List<string>{ "es_CL"},
                        SiteCode = "CL"
                    },
                     new SiteToCulture(){
                        CultureCodes = new List<string>{ "es_ES"},
                        SiteCode = "IB"
                    }
            };

            Extractor.ExtractSiteTranslations(siteCultureMaps, baseDirectory, outputDirectory, sitesToExtract, "\\app\\languages\\");

            foreach (var site in sitesToExtract)
            {
                Assert.True(Directory.Exists($@"{outputDirectory}{site}"));
                foreach (var lang in siteCultureMaps.SelectMany(i => i.LangCodes).Distinct())
                {
                    Assert.True(File.Exists($@"{outputDirectory}{site}\app\languages\{lang}.json"));
                }
            }

            var auJObject = Util.LoadAsJObject(Util.LoadFile($@"{outputDirectory}AU\app\languages\en.json"));
            Assert.True(auJObject["CHECKOUT_HEADER_INVALID_PROMO_CODE_1"].ToString() == "Oops! Your code does not exist. Please check and try again.");
            Assert.True(auJObject["CUSTOM_PRODUCT_STATUS_MESSAGE"].ToString() == "");
            var fsukJObject = Util.LoadAsJObject(Util.LoadFile($@"{outputDirectory}FSUK\app\languages\en.json"));
            Assert.True(fsukJObject["CHECKOUT_HEADER_INVALID_PROMO_CODE_1"].ToString() == "This promotion is not active for your warehouse");
            Assert.True(fsukJObject["CUSTOM_PRODUCT_STATUS_MESSAGE"].ToString() != "");
        }

        [Fact]
        public void ExtractSiteTranslationsFromCsv_AdminV5()
        {
            string sourceFileName = "BACKUP INTERNAL ONLY Master Translation Matrix - Admin V5_Matrix";

            string columnToExtract = "es-AR (Argentina)";
            int tagcolumnIndex = 1;
            var path = $@"..\\..\\..\\csv\{sourceFileName}.tsv";
            var destination = $@"..\\..\\..\\csv\output\{sourceFileName}_{columnToExtract}.json";

            if (File.Exists(destination))
            {
                File.Delete(destination);
            }

            Extractor.ExtractSiteTranslationsFromCsv(path, destination, columnToExtract, ExtractFromCsvType.JsonKeyValue, ExtractSeperator.Tab, tagcolumnIndex);

            Assert.True(File.Exists(destination));
        }

        [Fact]
        public void ExtractSiteTranslationsFromCsv_IdentityV5()
        {
            string sourceFileName = "BACKUP INTERNAL ONLY Master Translation Matrix - Identity V5_Matrix";

            string columnToExtract = "es-AR (Argentina)";
            int tagcolumnIndex = 1;
            var path = $@"..\\..\\..\\csv\{sourceFileName}.tsv";
            var destination = $@"..\\..\\..\\csv\output\{sourceFileName}_{columnToExtract}.json";

            if (File.Exists(destination))
            {
                File.Delete(destination);
            }

            Extractor.ExtractSiteTranslationsFromCsv(path, destination, columnToExtract, ExtractFromCsvType.JsonKeyValue, ExtractSeperator.Tab, tagcolumnIndex);

            Assert.True(File.Exists(destination));
        }

        [Fact]
        public void ExtractSiteTranslationsFromCsv_ShopeV5FE()
        {
            string sourceFileName = "ShopMatrix";

            string columnToExtract = "de-DE";
            int tagcolumnIndex = 1;
            var path = $@"..\\..\\..\\csv\{sourceFileName}.tsv";
            var destination = $@"..\\..\\..\\csv\output\{sourceFileName}_{columnToExtract}.json";

            if (File.Exists(destination))
            {
                File.Delete(destination);
            }

            Extractor.ExtractSiteTranslationsFromCsv(path, destination, columnToExtract, ExtractFromCsvType.JsonKeyValue, ExtractSeperator.Tab, tagcolumnIndex);

            Assert.True(File.Exists(destination));
        }

        [Fact]
        public void ExtractSiteTranslationsFromCsv_Android()
        {
            string sourceFileName = "BACKUP INTERNAL ONLY Master Translation Matrix - App Android_Matrix";

            string columnToExtract = "es-AR (Argentina)";
            int tagcolumnIndex = 1;
            var path = $@"..\\..\\..\\csv\{sourceFileName}.tsv";
            var destination = $@"..\\..\\..\\csv\output\{sourceFileName}_{columnToExtract}.xml";

            if (File.Exists(destination))
            {
                File.Delete(destination);
            }

            Extractor.ExtractSiteTranslationsFromCsv(path, destination, columnToExtract, ExtractFromCsvType.AndroidXml, ExtractSeperator.Tab, tagcolumnIndex);

            Assert.True(File.Exists(destination));
        }

        [Fact]
        public void ExtractSiteTranslationsFromCsv_iOS()
        {
            string sourceFileName = "BACKUP INTERNAL ONLY Master Translation Matrix - App iOS_Matrix";

            string columnToExtract = "es-AR (Argentina)";
            int tagcolumnIndex = 1;
            var path = $@"..\\..\\..\\csv\{sourceFileName}.tsv";
            var destination = $@"..\\..\\..\\csv\output\{sourceFileName}_{columnToExtract}.strings";

            if (File.Exists(destination))
            {
                File.Delete(destination);
            }

            Extractor.ExtractSiteTranslationsFromCsv(path, destination, columnToExtract, ExtractFromCsvType.iOS, ExtractSeperator.Tab, tagcolumnIndex);

            Assert.True(File.Exists(destination));
        }

        //[Fact]
        //public void MergeTranslations()
        //{
        //    var sourceFile = "ShopMatrix_de-DE";
        //    string source = Util.LoadFile(@$"C:\@Projects\Bidone\bdirect-translations-extractor\TranslationExtractor-Tests\csv\output\{sourceFile}.json");
        //    JObject sourceObject = Util.LoadAsJObject(source);

        //    var destinationFile = "de_DE";
        //    string destination = Util.LoadFile(@$"C:\Users\pavan.josyula\Downloads\{destinationFile}.json");
        //    JObject destinationObject = Util.LoadAsJObject(destination);

        //    destinationObject.Merge(sourceObject);
        //    //foreach (var key in destinationObject)
        //    //{
        //    //    if (sourceObject.TryGetValue(key.Key, out JToken? value) && string.IsNullOrWhiteSpace(value.ToString()))
        //    //    {
        //    //        destinationObject.Merge()
        //    //    }

        //    //}

        //    var finalstring = destinationObject.ToString();
        //    File.WriteAllText($@"C:\Temp\Translations\{destinationFile}.json", finalstring);

        //}
    }
}
