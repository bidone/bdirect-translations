using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using TranslationsExtractor;
using Xunit;

namespace TranslationExtractor_Tests
{
    public class GenaratorTests
    {
        [Fact]
        public void Test_Genarator()
        {
            var baseDirectory = $@"{System.AppDomain.CurrentDomain.BaseDirectory}source\";
            var overridesDirectory = $@"{System.AppDomain.CurrentDomain.BaseDirectory}source\overrides\";
            var outputDirectory = @$"{System.AppDomain.CurrentDomain.BaseDirectory}\genoutputs\";
            if (Directory.Exists(outputDirectory))
            {
                Directory.Delete(outputDirectory, true);

            }
            var siteCultureMaps = new List<SiteToCulture>()
            {
                    new SiteToCulture(){
                        CultureCodes = new List<string>{ "en"},
                        SiteCode = "AU"
                    },
                    new SiteToCulture(){
                        CultureCodes = new List<string>{ "en"},
                        SiteCode = "ZA"
                    },
                    new SiteToCulture(){
                        CultureCodes = new List<string>{ "en"},
                        SiteCode = "FSUK"
                    },
                    new SiteToCulture(){
                        CultureCodes = new List<string>{ "es_CL"},
                        SiteCode = "CL"
                    },
                     new SiteToCulture(){
                        CultureCodes = new List<string>{ "es_ES"},
                        SiteCode = "IB"
                    }
            };


            Generator.GenerateBaseLanguageFiles(baseDirectory, overridesDirectory, outputDirectory, siteCultureMaps);

            Assert.True(Directory.Exists(outputDirectory));

            foreach (var culture in siteCultureMaps.SelectMany(i => i.CultureCodes))
            {
                Assert.True(File.Exists($@"{outputDirectory}\{culture}.json"));
            }

            var enJObject = Util.LoadAsJObject(Util.LoadFile($"{outputDirectory}en.json"));
            Assert.True((enJObject.Properties().Any(i => i.Name.StartsWith("AU_") || i.Name.StartsWith("ZA_"))));

            var es_CLJObject = Util.LoadAsJObject(Util.LoadFile($"{outputDirectory}es_CL.json"));
            var es_ESJObject = Util.LoadAsJObject(Util.LoadFile($"{outputDirectory}es_ES.json"));
            Assert.True(es_CLJObject["ABOUT_COMPANY"].ToString() == "Sobre nosotros");
            Assert.True(es_ESJObject["ABOUT_COMPANY"].ToString() == "Con�cenos");
        }
    }
}
