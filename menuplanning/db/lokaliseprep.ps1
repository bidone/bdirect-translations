#This script is used rename the props from transifex propname to lokalise
$files = Get-ChildItem "..\..\menuplanning\db" -Recurse | Where-Object {$_.FullName.EndsWith(".json")}

foreach ($file in $files) {
  replace $file.FullName
}
# the original json text

function replace ($fileName){
Write-Host $fileName
$json = Get-Content $fileName

$json = $json.Replace('"string"', '"translation"').Replace('"character_limit"', '"limit"') | Out-File $fileName

} 